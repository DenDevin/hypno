<?php

namespace app\models;

use Yii;
use yii\helpers\Json;


class Books extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'books';
    }



    public function beforeSave($insert)
    {
        if(!empty($this->start_year))
        {
            $this->start_year = Yii::$app->formatter->asTimestamp($this->start_year);

        }
        return parent::beforeSave($insert);

    }

    public function rules()
    {
        return [
            [['author_id', 'rating'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'start_year'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'author_id' => 'Автор',
            'start_year' => 'Год издания',
            'rating' => 'Рейтинг',

        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }


    public static function find()
    {
        return new \app\models\query\BooksQuery(get_called_class());
    }


}
