<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string|null $titleurl
 * @property string|null $
 * @property int|null $author_id
 * @property int|null $project_id
 *
 * @property Projects $project
 */
class Authors extends \yii\db\ActiveRecord
{
    public $books;
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['title', 'birth_year'], 'string', 'max' => 255],
            [['rating'], 'integer'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Имя',
            'birth_year' => 'Год рождения',
            'sort' => 'Кол-во книг',
            'rating' => 'Рейтинг',
            'books' => 'Книги'

        ];
    }


    public function beforeSave($insert)
    {
        if(!empty($this->birth_year))
        {
            $this->birth_year = Yii::$app->formatter->asTimestamp($this->birth_year);

        }
        return parent::beforeSave($insert);

    }


    public function getBook()
    {
        return $this->hasMany(Books::className(), ['id' => 'author_id']);
    }

    public function getBookscount()
    {
        return $this->hasMany(Books::className(), ['author_id' => 'id'])->count();
    }


    public static function find()
    {
        return new \app\models\query\AuthorsQuery(get_called_class());
    }
}
