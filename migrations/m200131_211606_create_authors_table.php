<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%files}}`.
 */
class m200131_211606_create_authors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%authors}}', [

            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'birth_year' => $this->integer(),
            'rating' => $this->integer(),
            'active' => $this->boolean(),
            'sort' => $this->integer(),
            'updated_at' => $this->timestamp()->defaultValue(null),
            'created_at' => $this->timestamp(),


        ]);

    }

    public function safeDown()
    {
        $this->dropTable('{{%authors}}');
    }
}
