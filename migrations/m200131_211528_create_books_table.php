<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m200131_211528_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'start_year' => $this->string(),
            'author_id' => $this->integer(),
            'rating' => $this->integer(),
            'active' => $this->boolean(),
            'sort' => $this->integer(),
            'updated_at' => $this->timestamp()->defaultValue(null),
            'created_at' => $this->timestamp(),
        ]);

    }


    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
