<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AuthorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authors-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Authors', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',


            [
                'attribute' => 'birth_year',
                'format' => 'html',
                'value' => function($model)
                {
                    return Yii::$app->formatter->asDate($model->birth_year, 'dd.MM.yyyy');
                }

            ],

            [
                'attribute' => 'rating',
                'format' => 'html',
                'value' => function($model)
                {
                    return $model->rating;
                }

            ],

            [
                'attribute' => 'books',
                'format' => 'html',
                'value' => function($model)
                {
                    return Html::a('Books', Url::to(['authors/books', 'id' => $model->id]));
                }

            ],

            [
                'attribute' => 'sort',
                'format' => 'html',
                'value' => function($model)
                {
                    return $model->bookscount;
                }

            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
