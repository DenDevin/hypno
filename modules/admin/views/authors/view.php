<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Authors */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Authors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="authors-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'birth_year',
                'format' => 'html',
                'value' => function($model)
                {
                    return Yii::$app->formatter->asDate($model->birth_year, 'dd.MM.yyyy');
                }

            ],

            [
                'attribute' => 'books',
                'format' => 'html',
                'value' => function($model)
                {
                    return Html::a('Books', Url::to(['authors/books', 'id' => $model->id]));
                }

            ],
            'rating',
        ],
    ]) ?>

</div>
