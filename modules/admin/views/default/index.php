<div class="row">
    <div class="col-sm-6">
     <? foreach($books as $book) : ?>
         <div class="panel panel-success">
             <div class="panel-body">
                 <h4><?=$book->title?></h4>
             </div>
             <div class="panel-footer"><h5><?=$book->author->title?></h5></div>
         </div>
     <? endforeach; ?>
    </div>

    <div class="col-sm-6">
        <ul class="list-group">
            <? foreach($authors as $author) : ?>
            <li class="list-group-item"><?=$author->title?> <span class="badge badge-danger"><?=$author->bookscount?></span></li>
            <? endforeach; ?>
        </ul>

    </div>

</div>
