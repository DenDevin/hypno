<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */


$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="projects-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',

            [
                'attribute' => 'start_year',
                'format' => 'html',
                'value' => function($model)
                {
                    return Yii::$app->formatter->asDate($model->start_year, 'dd.MM.yyyy');
                }

            ],
            [
                'attribute' =>  'author_id',
                'format' => 'html',
                'value' => function($model)
                {
                    return $model->author->title;
                }

            ],
        ],
    ]) ?>

</div>
