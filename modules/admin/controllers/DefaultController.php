<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\models\Books;
use app\models\Authors;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $books = Books::find()->all();
        $authors = Authors::find()->all();
        return $this->render('index', compact('books', 'authors'));
    }
}
